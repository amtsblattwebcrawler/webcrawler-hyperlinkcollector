from bs4 import BeautifulSoup
import requests
import hashlib
from urllib.parse import urljoin
import boto3
from boto3.dynamodb.conditions import Key
from datetime import datetime
from botocore.exceptions import ValidationError
from uuid import uuid3, NAMESPACE_URL

#entrypoint to AWS
session = boto3.session.Session( region_name='eu-central-1',
                            aws_access_key_id='AKIAJHK4UKVNUFHNWRVQ',
                            aws_secret_access_key='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')

class Hyperlink_Collector:
    def __init__(self,parent_url,parent_url_id,city_name):
        self.db = session.resource('dynamodb')
        self.sqs = session.client('sqs')
        self. parent_url_id= parent_url_id
        self.parent_url = parent_url
        self.city_name=city_name

    def getPage(self):
        try:
            s = requests.Session()
            req = s.get(self.parent_url)
            return BeautifulSoup(req.text, features='html.parser')
        except requests.exceptions.RequestException:
            return None

    def isAbsoluteLink(self, link):
        if str(link).startswith("http://") or str(link).startswith("https://"):
            return True
        return False

    def resolveRelativeURL(self,parent_url,url):
        return urljoin(parent_url,url)

    def create_uuid (self,url):
        parent_url_enc = self.parent_url.encode('utf-8')
        url_enc = url.encode('utf-8')
        hashtring = parent_url_enc + url_enc
        url_id = hashlib.md5(hashtring).hexdigest()
        return url_id

    def create_uuid3(self,url):
        uuid= uuid3(NAMESPACE_URL, url)
        return str(uuid)

    def insert_hyperlink(self,url,link_text,url_id):
        self.db.Table('urlsList').put_item(
           Item= {
                "downloaded": False,
                "downloaded_on": "None",
                "full_url": str(url),
                "parent_url":str(self.parent_url),
                "link_text": str(link_text),
                "valid": True,
                "visited": False,
                "visited_on": "None",
                "downloaded_tries_number":0,
                "uuid": str(url_id)
            }
        )

    def mark_error (self):
        self.db.Table('seeds').update_item(
            Key={
                'ID': self.parent_url_id,
            },
            UpdateExpression="SET valid=:var1, history=:var2",
            ExpressionAttributeValues={
                ':var1': False,
                ':var2': str(datetime.utcnow().isoformat())+': '+' unable to get page'
            },
        )
        print('unable to get page:' + self.parent_url)

    def send_message_to_queue(self, url, url_id):
        self.sqs.send_message(
            QueueUrl="https://sqs.eu-central-1.amazonaws.com/419206837402/pdfURLqueue_v1",
            MessageBody=str(url),
            MessageAttributes={
                'ID': {
                    'StringValue': url_id,
                    'DataType': 'String'
                },
                'parent_URL_ID':{
                    'StringValue': self.parent_url_id,
                    'DataType': 'String'
                },
                'parent_URL':{
                    'StringValue': self.parent_url,
                    'DataType': 'String'
                },
                'cityName': {
                    'StringValue': self.city_name,
                    'DataType': 'String'
                }
            }
        )
        print(str(url)+':is added to queue')

    #improve only select url from the same parent URL
    def isURLDupilcate(self, url):
        table= self.db.Table('urlsList')
        res = table.query(
            KeyConditionExpression=Key('parent_url').eq(str(self.parent_url)) & Key('full_url').eq(str(url))
        )
        return True if len(res['Items'])!=0 else False

    def is_PDFFile(self,url):
        """
        Checking if document is a PDF File
        """
        try:
            h = requests.head(url, allow_redirects=True)
            header = h.headers
            content_type = header.get('content-type')
            if ('pdf' in content_type) or ('PDF' in content_type):
                return True
        except requests.exceptions.RequestException:
            return False

    def mark_last_crawled(self):
        self.db.Table('seeds').update_item(
            Key={
                'ID': self.parent_url_id,
            },
            UpdateExpression="SET #st =:var1",
            ExpressionAttributeValues={
                ':var1': str(datetime.utcnow().isoformat())
            },
            ExpressionAttributeNames= {
                '#st':'last_crawled'
            }
        )
        
    def collect(self):
        """
        Searches a given website for all links related to Amtsblatt and records all pages found
        """
        soup = self.getPage()
        if soup is not None:
            for link in soup.findAll('a'):
                text = str(link.getText().strip())
                href = str(link.get('href'))

                if self.isAbsoluteLink(href) is not True:
                    href = self.resolveRelativeURL(self.parent_url,href)

                if self.is_PDFFile(href) :
                    if self.isURLDupilcate(href) is False :
                        print(href)
                        doc_id= self.create_uuid3(href)
                        self.insert_hyperlink(href, text,doc_id)
                        self.send_message_to_queue(href,doc_id)
                        self.mark_last_crawled()


def main(event,context):
    print(event)
    for record in event['Records']:
        url = str(record['body'])
        payload = record['messageAttributes']
        cityURL_id = payload['parent_URL_ID']['stringValue']
        city_name = payload['Name']['stringValue']
        collector = Hyperlink_Collector(url,cityURL_id,city_name)
        result= collector.collect()
        if result is False:
            pass

if __name__== "__main__":
    event={
        'Records':[
            {
                'body':'https://www.kassel.de/aktuelles/aktuelles-inhalte/amtsblatt.php',
                'messageAttributes':{
                    'parent_URL_ID':
                        {'stringValue':'2ae48b65-67e2-50d8-8f15-227dd174ee2d'}
                }
            }
        ]
    }
    main(event, {})