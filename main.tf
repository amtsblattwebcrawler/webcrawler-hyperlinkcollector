provider "aws" {
  version = "2.14.0"
  region  = "eu-central-1"
  #access_key = "${var.aws_access_key}"
  #secret_key = "${var.aws_secret_key}"
  profile = "default"
}

resource "aws_s3_bucket" "terraform_state" {
  bucket = "iw-bigdata-demowebcrawler-tfstate"
  versioning {
    enabled = true
  }
  lifecycle {
    prevent_destroy = true
  }
}

terraform {
   backend "s3" {
   encrypt = true
   bucket = "iw-bigdata-demowebcrawler-tfstate"
   region = "eu-central-1"
   key = "iw-bigdata-demowebcrawler-tfstate/terraform.tfstate"
   profile = "default"
 }
}
output "s3_bucket_arn" {
  value = "${aws_s3_bucket.terraform_state.arn}"
}

data "aws_s3_bucket_object" "source_hash" {
  bucket = "demo-webcrawler-lambda-function"
  key    = "iw-bigdata-test2.base64sha256.txt"
}

data "aws_s3_bucket_object" "lambda" {
  bucket = "${var.BUCKET_NAME}"
  key    = "${var.KEY}"
}

//resource "aws_lambda_function" "iw-bigdata-test2" {
//  function_name    = "hyperlinkCollector"
//  role             = "arn:aws:iam::419206837402:role/service-role/MyLambdaExecutionRole"
//  handler          = "main.lambda_handler"
//  runtime          = "python3.6"
//  s3_bucket        = "${data.aws_s3_bucket_object.lambda.bucket}"
//  s3_key           = "${data.aws_s3_bucket_object.lambda.key}"
//  source_code_hash = "${data.aws_s3_bucket_object.source_hash.body}"
//}

resource "aws_lambda_event_source_mapping" "example" {
  event_source_arn = "arn:aws:sqs:eu-central-1:419206837402:iw-bigdata-demowebcrawler-cityURLqueue_test"
  function_name    = "arn:aws:lambda:eu-central-1:419206837402:function:hyperlinkCollector"
}
